// cooldown.js
const { Collection } = require('discord.js');

// A collection to store the cooldowns
const cooldowns = new Collection();

// A function to check the cooldown
function checkCooldown(command, interaction) {
  // If the command has no cooldown, return false
  if (!command.cooldown) return false;

  // Get the user's ID
  const userId = interaction.user.id;

  // Get the current timestamp
  const now = Date.now();

  // If the cooldowns collection does not have the command name, create a new collection for it
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Collection());
  }

  // Get the collection for the command name
  const timestamps = cooldowns.get(command.name);

  // Get the cooldown duration in milliseconds
  const cooldownAmount = command.cooldown * 1000;

  // If the timestamps collection has the user's ID, check the cooldown
  if (timestamps.has(userId)) {
    // Get the expiration time of the cooldown
    const expirationTime = timestamps.get(userId) + cooldownAmount;

    // If the current time is before the expiration time, return true
    if (now < expirationTime) {
      return true;
    }
  }

  // Otherwise, set the user's ID and the current timestamp in the timestamps collection
  timestamps.set(userId, now);

  // Return false
  return false;
}

// Export the function
module.exports = checkCooldown;