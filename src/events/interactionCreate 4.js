const { Interaction, Collection } = require("discord.js");
const config = require("../config");
const cooldowns = new Collection();

// ... Your other code

module.exports = {
  name: 'interactionCreate',
  async execute(interaction, client) {
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);

    if (!command) return;

    // Check for command cooldown
    if (command.options?.cooldown) {
      const userId = interaction.user.id;
      const ignoredUserId = '741211030427926629'; // Replace 'specific-user-id' with the actual user ID to be ignored

      if (userId !== ignoredUserId) {
        const userCooldowns = cooldowns.get(userId) || new Set();

        if (userCooldowns.has(interaction.commandName)) {
          await interaction.reply({
            content: config.messageSettings?.cooldownMessage || "Slow down buddy! You're too fast to use this command",
          });
          return;
        }

        userCooldowns.add(interaction.commandName);
        cooldowns.set(userId, userCooldowns);

        setTimeout(() => {
          userCooldowns.delete(interaction.commandName);
          if (userCooldowns.size === 0) {
            cooldowns.delete(userId);
          }
        }, command.options.cooldown);
      }
    }
    try {
      await command.execute(interaction, client);
    } catch (error) {
      console.log(error);
      await interaction.reply({
        content: 'There was an error while executing this command!',
        ephemeral: true,
      });
    }
  },
};
