// Import the SlashCommandBuilder class from discord.js
const { SlashCommandBuilder } = require('discord.js');

// Define a utility function to format a date object into a human-readable string
function formatDate(date) {
  return new Intl.DateTimeFormat('en-US').format(date);
}

// Define a utility function to get the icon URL of a guild
function getIconURL(guild) {
  return guild.iconURL({ dynamic: true, size: 4096 });
}

// Export the data and execute properties of the slash command
module.exports = {
  // Create the command definition using the builder
  data: new SlashCommandBuilder()
    .setName('serverinfo')
    .setDescription('Shows some information about the server'),

  // Define the function to run when the command is used
  async execute(interaction) {
    // Get the guild object from the interaction
    const guild = interaction.guild;

    // Create an embed object to store the response
    const embed = {
      title: guild.name,
      url: getIconURL(guild),
      thumbnail: {
        url: getIconURL(guild),
      },
      fields: [
        {
          name: 'Owner',
          value: `<@${guild.ownerId}>`,
          inline: true,
        },
        {
          name: 'Members',
          value: guild.memberCount.toString(),
          inline: true,
        },
        {
          name: 'Roles',
          value: guild.roles.cache.size.toString(),
          inline: true,
        },
        {
          name: 'Channels',
          value: guild.channels.cache.size.toString(),
          inline: true,
        },
        {
          name: 'Created at',
          value: formatDate(guild.createdAt),
          inline: true,
        },
      ],
      timestamp: new Date(),
      footer: {
        text: 'Powered by Elysia',
      },
    };

    // Reply with the embed object
    await interaction.reply({ embeds: [embed] });
  },
};