const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('help')
    .setDescription('Get information about Elysia'),

  async execute(interaction) {
    await interaction.reply(
      "Hello, I'm Elysia! I'm made by the Elysia Api and developers! " +
      "I'm based on GPT 4 so you can ensure you have the latest information on the world, and your favorite sports! " +
      "I will not be verified, so if I leave your server, make sure to re-invite me again!"
    );
  },
};

