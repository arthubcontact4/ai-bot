const { SlashCommandBuilder } = require("@discordjs/builders");
const { EmbedBuilder, ChannelType } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("webhook")
    .setDescription("Manage webhooks")
    .addSubcommand((subcommand) =>
      subcommand
        .setName("create")
        .setDescription("Create a webhook")
        .addChannelOption((option) =>
          option
            .setName("channel")
            .setDescription("The channel to create the webhook in")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("name")
            .setDescription("The name of the webhook")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("avatar")
            .setDescription("The avatar URL of the webhook")
        )
    )
    .addSubcommand((subcommand) =>
      subcommand
        .setName("delete")
        .setDescription("Delete a webhook")
        .addStringOption((option) =>
          option
            .setName("id")
            .setDescription("The ID of the webhook to delete")
            .setRequired(true)
        )
    )
    .addSubcommand((subcommand) =>
      subcommand
        .setName("edit")
        .setDescription("Edit a webhook")
        .addStringOption((option) =>
          option
            .setName("id")
            .setDescription("The ID of the webhook to edit")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option.setName("name").setDescription("The new name of the webhook")
        )
        .addStringOption((option) =>
          option
            .setName("avatar")
            .setDescription("The new avatar URL of the webhook")
        )
    )
    .addSubcommand((subcommand) =>
      subcommand
        .setName("view")
        .setDescription("View webhooks in a channel")
        .addChannelOption((option) =>
          option
            .setName("channel")
            .setDescription("The channel to view webhooks in")
            .setRequired(true)
        )
    ),
  async execute(interaction) {
    await interaction.deferReply({ ephemeral: true });
    const owners = ['741211030427926629']

    if (!owners.includes(interaction.user.id)) return interaction.editReply(`🔒 You must be an owner to use this command!`)
    const subcommand = interaction.options.getSubcommand();
    if (subcommand === "create") {
      const channel = interaction.options.getChannel("channel");
      const name = interaction.options.getString("name");
      const avatar = interaction.options.getString("avatar");
      if (channel.type === ChannelType.GuildText || channel.type === ChannelType.GuildVoice || channel.type === ChannelType.GuildCategory || channel.type === ChannelType.GuildNews || channel.type === ChannelType.GuildAnnouncement)   {
        const webhook = await channel.createWebhook({
          name: name,
          avatar: avatar,
        });
        await interaction.editReply({ content: `Webhook created with ID: ${webhook.id}`, ephemeral: true });
      } else {
        await interaction.editReply({ content: `Please select a text-based channel.`, ephemeral: true });
      }
    } else if (subcommand === "delete") {
      const id = interaction.options.getString("id");
      try {
        const webhook = await interaction.client.fetchWebhook(id);
        await webhook.delete();
        await interaction.editReply({ content: `Webhook Deleted.`, ephemeral: true });
      } catch (error) {
        await interaction.editReply(
          `An error occurred while trying to delete the webhook: ${error.message}`
        );
        console.log(id)
      }
    } else if (subcommand === "edit") {
      const id = interaction.options.getString("id");
      const name = interaction.options.getString("name");
      const avatar = interaction.options.getString("avatar");
      try {
        const webhook = await interaction.client.fetchWebhook(id);
        await webhook.edit({ name, avatar });
        await interaction.editReply({ content: `Webhook Updated.`, ephemeral: true });
      } catch (error) {
        await interaction.editReply(
          `An error occurred while trying to update the webhook: ${error.message}`
        );
      }
    } else if (subcommand === "view") {
      const channel = interaction.options.getChannel("channel");
      if (channel.type === ChannelType.GuildText || channel.type === ChannelType.GuildVoice || channel.type === ChannelType.GuildCategory || channel.type === ChannelType.GuildNews || channel.type === ChannelType.GuildAnnouncement) {
        const webhooks = await channel.fetchWebhooks();
        if (webhooks.size > 0) {
          const embed = new EmbedBuilder()
            .setTitle(`Webhooks in #${channel.name}`)
            .setDescription(
              webhooks
                .map((webhook) => `ID: ${webhook.id} | Token: ${webhook.token} | Name: ${webhook.name} | Url: https://discord.com/api/webhooks/${webhook.id}/${webhook.token}`)
                .join("\n")
            );
          await interaction.editReply({ embeds: [embed], ephemeral: true });
        } else {
          await interaction.editReply(`There are no webhooks in #${channel.name}.`);
        }
      } else {
        await interaction.editReply({ content: `Please select a text-based channel.`, ephemeral: true });
      }
    }
  },
};
