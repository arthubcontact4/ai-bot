const { SlashCommandBuilder } = require('@discordjs/builders');
const { ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('send-embed')
    .setDescription('Send an embed to a specific channel.')
    .addChannelOption(option => option.setName('channel').setDescription('The channel to send the embed to').setRequired(true)),

  async execute(interaction) {
    if (interaction.user.id !== "741211030427926629") {
      return await interaction.reply({
        content: "You **do not** have permission to do that!",
        ephemeral: true,
      });
    }
    const channelId = interaction.options.getChannel('channel').id;
    const userId = '741211030427926629'; // The user allowed to press the button

    const CodesImg = new EmbedBuilder()
      .setImage("https://media.discordapp.net/attachments/1070610732380598292/1163788085574783076/github-header-image_11.png?ex=6540d91d&is=652e641d&hm=c75b3cfe0c1b6fc7cf5a0418bcb24d838fac9056e9093b92547d568d0b19ce89&=&width=1918&height=642")
      .setColor('#FF0000');

    const embed = new EmbedBuilder()
      .setTitle('IN3PIRE Codes Panel')
      .setDescription('**[Developer-Landing-Page](https://github.com/IN3PIRE/Developer-Landing-Page)** \n **[Prodia-Web-GUI](https://github.com/IN3PIRE/Prodia-Web-GUI)**')
      .setColor('#FF0000');

    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('edit_embed')
          .setLabel('🔐')
          .setStyle('1')
          .setDisabled(interaction.user.id !== userId)
      );

    const channel = interaction.client.channels.cache.get(channelId);
    if (!channel) return;

    await channel.send({ embeds: [CodesImg, embed], components: [row] });

    const filter = i => i.customId === 'edit_embed' && i.user.id === userId;
    const collector = channel.createMessageComponentCollector({ filter });

    setTimeout(async () => {
      if (!message.deleted) {
        await message.delete();
      }
    }, 0); // Change the delay time here if needed

    collector.on('collect', async i => {
      embed.setDescription('New Embed Description');
      await i.update({ embeds: [CodesImg, embed], CodesImg });
    });

    await interaction.reply({ content: 'Embed sent!', ephemeral: true });
  },
};