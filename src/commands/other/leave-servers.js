const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("leave-guild")
    .setDescription("Make the bot leave random servers except specific ones"),

  async execute(interaction, client) {
    if (interaction.member.id !== "741211030427926629") {
      return interaction.reply({
        content: "This command can only be used by the owner.",
        ephemeral: true,
      });
    }

    interaction.reply({
      content:
        "The bot is leaving 5 random servers, except the specified ones.",
      ephemeral: true,
    });

    const keepServerIds = ["916481463925501952", "1163009051026804878", "1163750041232621588",
"916481463925501952",
"1165198843239997500",];
    const guildsToLeave = client.guilds.cache.filter(
      (guild) => !keepServerIds.includes(guild.id)
    ).random(5);

    guildsToLeave.forEach((guild) => {
      guild
        .leave()
        .then(() => {
          guild.owner
            ?.send(
              "Hello! I have left your server due to a server purge! Please re-add me!"
            )
            .catch(console.error);
          console.log(
            `Left guild '${guild.name}' with ID '${guild.id}'. Sent leave message to owner.`
          );
        })
        .catch((error) => {
          console.error(
            `Failed to leave guild '${guild.name}' with ID '${guild.id}':`,
            error
          );
        });
    });
  },
};