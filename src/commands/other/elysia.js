const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder, WebhookClient, Collection } = require('discord.js');
const axios = require('axios')
const fs = require('fs');
//const authorizedUserIds = require('./authorized-users.json');
const cooldowns = new Collection();
// Im busy dont run


module.exports = {
  data: new SlashCommandBuilder()
    .setName('elysia')
    .setDescription('Talk with Elysia!')
    .addStringOption(option =>
      option.setName('message')
        .setDescription('What do you want to ask Elysia?')
        .setRequired(true)
    ),
  options: {
    cooldown: 20000, // 5 seconds cooldown for the "example" command
  },
  async execute(interaction) {





    /*  if (!authorizedUserIds.includes(interaction.user.id)) {
      return interaction.reply({
        content: '*** ⛔️Due to ratelimits, you must be an authorized user! Please dm `trivee.` for access!`!***',
        ephemeral: true,
      });
    } */

    //   const { user } = interaction;
    //    const userid = user.id;

    //    if (!authorizedUserIds.includes(userid)) {
    //    return interaction.reply("Sorry, you are not authorized to execute this command.");
    // }

    // Continue with executing the slash command logic for authorized users
    // Continue with executing the slash command logic for authorized users

    // Example response for an authorized user



    /*
    const blacklistedUser = await Blacklist.findOne({ userId });
 
    if (blacklistedUser) {
      return await interaction.reply('You are blacklisted and cannot use commands.');
    }
    } catch (error) {
      console.error('Error occurred while checking blacklist:', error);
      await interaction.reply('An error occurred while checking your blacklist status.');
    }
    */


    // const message = interaction.options.getString('message');


    const webhookUrl = 'https://discord.com/api/webhooks/1161202585999515648/YEy87C7mtiD3pDgioG--By9YTMRR6Y4r6G_syBoq-wfn6cX5RQioOMLHHHyJSYYH54rA';

    const message = interaction.options.getString('message');
    const userId = interaction.member.user.id;

    const embed = {
      title: 'Command Execution',
      description: `**User:** <@${userId}>\n**Message:** ${message}`,
    };

    const webhookClient = new WebhookClient({ url: webhookUrl });
    webhookClient.send({ embeds: [embed] });
    /*
    const webhookURL = "https://discord.com/api/webhooks/1161202585999515648/YEy87C7mtiD3pDgioG--By9YTMRR6Y4r6G_syBoq-wfn6cX5RQioOMLHHHyJSYYH54rA"; // Replace this with your webhook URL
    const data = {
      content: message,
      username: interaction.user.id
    };
    fetch(webhookURL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    */


    /* const hasEmojis = /[\u{1F000}-\u{1F6FF}\u{1F900}-\u{1F9FF}\u{2600}-\u{26FF}\u{2700}-\u{27BF}\u{2300}-\u{23FF}\u{1F300}-\u{1F5FF}\u{1F680}-\u{1F6FF}]/u.test(message);
 
     if (hasEmojis) {
       await interaction.reply('Emojis are not allowed in the message.');
       return;
     } */

    // Add the initial reply with "Cooking up some methpain..."
    const initialReply = await interaction.reply('<a:ai:1162877380868714497> **Generating your Response**');

    //https://api.freegpt4.ddns.net/

    //const tlnxURL = `https://api.freegpt4.ddns.net?text=${encodeURIComponent(message)}`;

    const tlnxURL = `https://gpt4-uakw.onrender.com/?text=${encodeURIComponent(message)}`;

    //?style=precise
    //https://gpt4-uakw.onrender.com

    try {
      const response = await axios.get(tlnxURL);
      let content = response.data;



      content = "***⚠️ Elysia might give inaccurate information or nsfw, if you encounter any errors. Please report them to `trivee.`*** **!** \n \n " + content;
      content = content.replace(/:blush:/gi, '<:PS_ASPOGGIES:1012972967128481852>');
      content = content.replace(/😊/gi, '<:PS_ASPOGGIES:1012972967128481852>');
      content = content.replace(/Bing/gi, 'Elysia');
      content = content.replace(/\[(\d+)\]\[(\d+)\]/gi, '.');
      content = content.replace(/Microsoft/gi, 'Elysia Team');
      content = content.replace(/\[(\d+)\]\[(\d+)\]/gi, '.');
      content = content.replace(/[]/gi, ' ')
      content = content.replace(/^/gi, ' ')
      // [] regex here
      content = content.replace(/\[(.*?)\]:  "" /g, '- ');

      content = content.replace(/\[\^(\d+)\^\]\[(\d+)\]/gi, function(match, group1, group2) {
        return `[${group1}][${group2}]`;  // Replace with your desired variable or regex pattern
      });
      //content = content.replace(/([^3])3/g, '$1 ');
      content = content.replace(/Hello, this is Elysia. My last training stopped in 2021. Why do you ask?/gi, 'Hello, this is Elysia. My last training stopped in 2023. Why do you ask?')

      // Create an embed with the edited content




      const maxChars = 1000; // Maximum characters per embed description

      // If content length is below or equal to maxChars, send a single embed
      //STARTS HERE
      /*
          if (content.length <= maxChars) {
            const embed = new EmbedBuilder()
              .setDescription(content);
            await interaction.editReply({ embeds: [embed] });
          } else {
            const embeds = [];
            while (content.length > 0) {
              const description = content.substring(0, maxChars);
              const embed = new EmbedBuilder()
                .setDescription(description);
              embeds.push(embed);
              content = content.substring(maxChars);
            }
            await interaction.editReply({ embeds: embed });
          }
 
          // Edit the initial reply with the embed and remove the "Cooking up some methpain..." message
          await initialReply.edit({
            content: '',
            embeds: [embeds]
 
          }); */
      //ENDS HERE


      if (content.length <= maxChars) {
        const embed = new EmbedBuilder()
          .setDescription(content);
        await interaction.editReply({ embeds: [embed] });
      } else {
        const embeds = [];
        while (content.length > 0) {
          const description = content.substring(0, maxChars);
          const embed = new EmbedBuilder()
            .setDescription(description);
          embeds.push(embed);
          content = content.substring(maxChars);
        }
        // Edit the initial reply with the embeds and remove the "Cooking up some methpain..." message
        await initialReply.edit({
          content: '',
          embeds: embeds
        });
      }


    } catch (error) {
      console.error(error);


    }
  }
};





/*
 // Check if the content exceeds 1000 characters
 if (content.length <= 1000) {
   const embed = new EmbedBuilder().setDescription(content);
   await interaction.editReply({ embeds: [embed] });
 } else {
   // Split the content into chunks of 1000 characters or less
   const chunks = content.match(/[\s\S]{1,1000}/g);

   for (let i = 0; i < chunks.length; i++) {
     const embed = new EmbedBuilder().setDescription(chunks[i]);
     // If it's the last chunk, edit the original reply
     if (i === 0) {
       await interaction.editReply({ embeds: [embed] });
     } else {
       // Send follow-up messages for subsequent chunks
       await interaction.followUp({ embeds: [embed] });

      await initialReply.edit({
         content: '',
         embeds: [embed]

       }); 
     }
   }
 }
}

catch (error) {
 console.error(error);
 await interaction.editReply('An error occurred while retrieving the content.');
}
}
};
*/