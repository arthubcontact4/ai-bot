const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('dev')
    .setDescription('Dev Only')
    .addStringOption(option =>
      option
        .setName('code')
        .setDescription('The code to execute')
        .setRequired(true)
    )
    .addStringOption(option =>
      option
        .setName('color')
        .setDescription('The color for the embed')
        .setRequired(false)
    ),

  async execute(interaction) {
    if (interaction.user.id !== "741211030427926629") {
      return await interaction.reply({
        content: "You **do not** have permission to do that!",
        ephemeral: true,
      });
    }
    const text = interaction.options.getString('code');
    const color = interaction.options.getString('color') || getRandomColor();

    const url = `https://flamingtext.com/net-fu/proxy_form.cgi?script=smurfs-logo&text=${encodeURIComponent(text)}&textcolor=${encodeURIComponent(color)}&_loc=generate&imageoutput=true`;

    await interaction.reply(url);
  },
};

function getRandomColor() {
  const colors = ['red', 'blue', 'green', 'yellow', 'orange', 'purple'];
  return colors[Math.floor(Math.random() * colors.length)];
}