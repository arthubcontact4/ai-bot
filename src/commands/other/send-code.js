  const {SlashCommandBuilder, CommandInteraction, ModalBuilder, ActionRowBuilder, TextInputStyle, TextInputBuilder, EmbedBuilder} = require('discord.js');

  module.exports = {
      data: new SlashCommandBuilder()
      .setName("send-code")
      .setDescription("Easily create an embed")
      .addChannelOption(option => option.setName('channel').setDescription("Channel to send the code").setRequired(true))
      .addBooleanOption(option => option.setName('use-webhook').setDescription("Whether to use webhook or not"))
      .addStringOption(option => option.setName('webhook-link').setDescription("IF use-webhook is True then link of the Webhook to use.")),
      /**
       * 
       * @param {CommandInteraction} interaction 
       */
      async execute (interaction) {
        if (interaction.user.id === `741211030427926629`) {
          const modal = new ModalBuilder()
          .setTitle("Embed Builder Menu")
          .setCustomId(`embed-menu`)
          .setComponents([
              new ActionRowBuilder().addComponents(new TextInputBuilder().setRequired(false).setCustomId('title').setLabel('Embed Title').setMaxLength(1023).setStyle(TextInputStyle.Paragraph)),
              new ActionRowBuilder().addComponents(new TextInputBuilder().setRequired(false).setCustomId('desc').setLabel('Embed Description').setMaxLength(3999).setStyle(TextInputStyle.Paragraph)),
              new ActionRowBuilder().addComponents(new TextInputBuilder().setRequired(false).setCustomId('color').setLabel('Embed Color Hex').setValue('#ff0000').setMaxLength(8).setStyle(TextInputStyle.Short)),
              new ActionRowBuilder().addComponents(new TextInputBuilder().setRequired(false).setCustomId('image').setLabel('Embed Image').setStyle(TextInputStyle.Short)),
              new ActionRowBuilder().addComponents(new TextInputBuilder().setRequired(false).setCustomId('thumb').setLabel('Embed Thumbnail').setStyle(TextInputStyle.Short))
          ]);
          interaction.showModal(modal);

          interaction.awaitModalSubmit({filter: (i) => i.user.id == interaction.user.id && i.customId == 'embed-menu', time: 900*1000})
          .catch(e => {})
          .then(async i => {
              if(!i) return;
              const embed = new EmbedBuilder()
              .setTitle(i.fields.getTextInputValue('title') || null)
              .setDescription(i.fields.getTextInputValue('desc') || null)
              .setColor(i.fields.getTextInputValue('color'))
              .setImage(i.fields.getTextInputValue('image') || null)
              .setThumbnail(i.fields.getTextInputValue('thumb') || null)

              await i.reply({content: "Sending embed ...", ephemeral: true});

              if(!interaction.options.getBoolean('use-webhook')) interaction.options.getChannel('channel')?.send({embeds: [embed]}).then(e => i.editReply("Embed has been Sent")).catch(e => {
                  interaction.options.getChannel('channel')?.send({embeds: [embed.setDescription('_ _').setColor('Aqua')]}).then(e => i.editReply("Embed has been Sent")).catch(e => {i.editReply("There was an issue while sending the embed."); console.log(e)})
              });
              else {
                  if(!interaction.guild.members.me.permissions.has("ManageWebhooks")) i.editReply("No permission to manage webhooks");
                  else {
                      let webs = await interaction.guild.fetchWebhooks()
                      let cweb = webs.find(i => i.url == interaction.options.getString('webhook-link'))
                      if(cweb){
                          cweb.send({embeds:[embed]}).then(e => i.editReply("Embed has been Sent")).catch(e => {
                              cweb.send({embeds:[embed.setDescription('_ _').setColor('Aqua')]}).then(e => i.editReply("Embed has been Sent")).catch(e => {i.editReply("There was an issue while sending the embed."); console.log(e)})
                          });
                      }else{
                          i.editReply("Provided Webhook cannot be found.");
                      }
                  }
              }
          });
      }
  }
  }