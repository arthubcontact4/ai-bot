const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const axios = require('axios'); // import axios module

module.exports = {
  data: new SlashCommandBuilder()
    .setName('restart')
    .setDMPermission(false)
    .setDescription('Shuts down Elysia. Only the owner of Elysia can use this command.'),
  async execute(interaction, client) {
    if (interaction.user.id === `741211030427926629`) {
      await interaction.reply({ content: `** Shutting down...**`, ephemeral: true })
      await client.user.setStatus("invisible")
      await axios.get('https://api.render.com/deploy/srv-ckmhr2rj89us73dpbkgg?key=PktWnlJRZcM') // use axios to make a GET request to the URL
      const embed = new EmbedBuilder()
        .setTitle('Elysia Restarted') // set the title of the embed
        .setColor('#00ff00') // set the color of the embed
        .setDescription('The API and bot have been restarted successfully.'); // set the description of the embed
      // send the embed to a specific channel by ID
      await client.channels.cache.get('1165467124127514684').send({ embeds: [embed] });
      process.exit();
    } else {
      return interaction.reply({ content: `Only **the owner** of Elysia can use this command.`, ephemeral: true })
    }
  }
}
