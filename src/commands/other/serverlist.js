const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('server-list')
    .setDescription('Shows all the servers Elysia is in'),
  async execute(interaction, client) {

    let guilds = await client.guilds.fetch()

    const embed = new EmbedBuilder()
      .setDescription(`*Elysia is in* **${client.guilds.cache.size}** *servers including:*`)
      .setColor("Blue")
      .addFields({ name: " ", value: guilds.map(g => `- ${g.name}`).join("\n") })
      .setFooter({ text: "Elysia", iconURL: 'https://cdn.discordapp.com/avatars/1062376114959425566/ab6ca2eb77357023e24958334d06fb59.webp' })

    await interaction.reply({ embeds: [embed] });
  },
};