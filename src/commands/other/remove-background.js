const { SlashCommandBuilder, EmbedBuilder, AttachmentBuilder } = require('discord.js')
const { removeBackground } = require('transparent-bg')

module.exports = {
  data: new SlashCommandBuilder()
    .setName('remove-background')
    .setDescription('Remove the background from an image.')
    .addAttachmentOption(option =>
      option.setName('image')
        .setDescription('Select the image for the background to be removed.')
        .setRequired(true)),
  async execute(interaction) {
    await interaction.deferReply({ ephemeral: true });
    const image = interaction.options.getAttachment('image');
    const response = await fetch('https://api.remove.bg/v1.0/removebg', {
      method: 'POST',
      headers: {
        'X-Api-Key': 'FXp7UwoiVbkwQgrZtmasN3L4',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        image_url: image.proxyURL,
        size: 'auto'
      })
    });

    const arrayBuffer = await response.arrayBuffer()
    const buffer = Buffer.from(arrayBuffer)
    const attachment = new AttachmentBuilder(buffer, { name: 'removebg.png' })

    const embed = new EmbedBuilder()
      .setColor('Random')
      .setTitle('Removed your images background')
      .setImage('attachment://removebg.png')

    await interaction.editReply({ embeds: [embed], files: [attachment], ephemeral: true })

  }
}
