const { Client, GatewayIntentBits, EmbedBuilder, PermissionsBitField, Permissions, MessageManager, Embed, Collection, ActivityType } = require(`discord.js`);
const fs = require('fs');
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });
const cooldowns = new Collection();
const { create } = require('sourcebin');

client.commands = new Collection();

require('dotenv').config();

const functions = fs.readdirSync("./src/functions").filter(file => file.endsWith(".js"));
const eventFiles = fs.readdirSync("./src/events").filter(file => file.endsWith(".js"));
const commandFolders = fs.readdirSync("./src/commands");

(async () => {
  for (file of functions) {
    require(`./functions/${file}`)(client);
  }
  client.handleEvents(eventFiles, "./src/events");
  client.handleCommands(commandFolders, "./src/commands");
  client.login(process.env.token)
})();

const time = Date.now(); require('http').createServer((req, res) => { res.write('Hello World!'); res.end(); }).listen(80, () => { console.log("Started"); console.log(`Time Taken: ${Date.now() - time}ms`) })


const colors = require('colors')
setTimeout(async () => {
  if (!client || !client.user) {
    console.log("Cient didn't logged in.. Killing the process..".america)
    process.kill(1);
  } else {
    console.log("Client has succesfully logged in!".rainbow)
  }
}, 1 * 1000 * 20);


/*
setTimeout(() => {
  process.exit(); // Terminate the Node.js process
}, 30000); // 30 seconds
*/

const express = require('express');
const app = express();
const adminPassword = process.env.ADMIN_PASSWORD;

app.get('/manageusers/:userId/:action', (req, res) => {
  const { userId, action } = req.params;
  const { password } = req.query;

  if (password !== adminPassword) {
    return res.status(401).json({ message: 'Unauthorized access.' });
  }

  let authorizedUsers = getAuthorizedUsers();

  if (action === 'add') {
    if (authorizedUsers.includes(userId)) {
      return res.status(400).json({ message: `User ID ${userId} already exists.` });
    }

    authorizedUsers.push(userId);
    writeAuthorizedUsers(authorizedUsers);

    return res.status(200).json({ message: `User ID ${userId} added successfully.` });
  }

  if (action === 'remove') {
    const index = authorizedUsers.indexOf(userId);

    if (index === -1) {
      return res.status(400).json({ message: `User ID ${userId} not found.` });
    }

    authorizedUsers.splice(index, 1);
    writeAuthorizedUsers(authorizedUsers);

    return res.status(200).json({ message: `User ID ${userId} removed successfully.` });
  }

  return res.status(400).json({ message: 'Invalid action.' });
});

function getAuthorizedUsers() {
  const authorizedUsersRaw = fs.readFileSync('./src/commands/other/authorized-users.json');
  return JSON.parse(authorizedUsersRaw);
}

function writeAuthorizedUsers(authorizedUsers) {
  fs.writeFileSync('./src/commands/other/authorized-users.json', JSON.stringify(authorizedUsers, null, 2));
}

function getAuthorizedUsers() {
  try {
    const authorizedUsersRaw = fs.readFileSync('./src/commands/other/authorized-users.json');
    return JSON.parse(authorizedUsersRaw) || [];
  } catch (error) {
    console.error('Error reading authorized-users.json:', error);
    return [];
  }
}

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});

// Create an event listener for messages
client.on("messageCreate", (message) => {
  // Ignore messages from bots
  if (message.author.bot);


  // If the message is a ping to the bot
  if (message.content == `<@${client.user.id}>`) {

    // Create an embed with some information
    const embed = new EmbedBuilder()
      .setColor("#0099ff")
      .setTitle("I have been pinged!")
      .setDescription(":crescent_moon: Good day! I am **Elysia**, an insightful Discord bot offering **free access to GPT-4**! With my internet connectivity, I can provide you with the most up-to-date information available. Excited to get started? Begin your journey by typing </Elysia:1162869463704678480> and enjoy the experience!")

    // Send the embed to the same channel as the message
    message.channel.send({ embeds: [embed] });
  }
});


const axios = require("axios");


// Listener for messageCreate event
client.on("messageCreate", async (message) => {
  if (message.author.bot) return;

  // Define the userIdToListen and channelIds if not already defined
  const userIdToListen = '741211030427926629';
  const channelIds = ['1184426085136474183', '1184426085136474183']; // Add your channel IDs here

  if (message.author.id !== userIdToListen) {
    await message.channel.send("Please execute `/elysia` to use me!");
    return;
  }

  // Check if the message is from one of the specified channels
  if (channelIds.includes(message.channel.id)) {
    const tlnxURL = `https://gpt4.zc.al/?text=${encodeURIComponent(
      message.content
    )}`;

    try {
      const response = await axios.get(tlnxURL);
      let reply = response.data; // Assuming the API response is the message content

      // Your content modifications here...

      if (reply.length > 2000) {
        try {
          // Import or require the create function from Sourcebin at the beginning of your file
          const bin = await create({
            title: 'Tu respuesta',
            description: 'Hecho por Pokémon Kingdom',
            files: [{
              content: reply,
              language: 'js',
            }],
          });

          // Send the Sourcebin URL in the message reply
          await message.reply(bin.url, {
            allowedMentions: {
              parse: [],
              repliedUser: false,
              everyone: false,
              roles: false
            }
          });
        } catch (error) {
          console.error('An error occurred while using Sourcebin:', error);
          await message.reply('Un error al usar Sourcebin.');
        }
      } else {
        // Send the reply directly if it's under the length limit
        await message.reply(reply, {
          allowedMentions: {
            parse: [],
            repliedUser: false,
            everyone: false,
            roles: false
          }
        }).catch(console.error);
      }
    } catch (e) {
      console.error('An error occurred while fetching the response:', e);
      await message.channel.send('An error occurred while processing your request.');
    }
  }
});